'use strict';

var utils = require('../utils/writer.js');
var Shortinfo = require('../service/ShortinfoService');

module.exports.shortinfoGET = function shortinfoGET(
  req,
  res,
  next,
  id,
  isDeleted
) {
  console.log(id);
  Shortinfo.shortinfoGET(id, isDeleted)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.shortinfoNameGET = function shortinfoNameGET(
  req,
  res,
  next,
  name
) {
  Shortinfo.shortinfoNameGET(name)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
