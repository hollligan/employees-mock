FROM node:lts-alpine

# создание директории приложения
WORKDIR /usr/src/app

# скопировать оба файла: package.json и package-lock.json
COPY package*.json ./

RUN npm ci --only=production
RUN npm cache clean --force 

# копируем исходный код
COPY . .

EXPOSE 8080
CMD [ "node", "index.js" ]