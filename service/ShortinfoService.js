'use strict';

/**
 * получение информации о сотруднике
 *
 * id List массив id сотрудников
 * isDeleted Boolean
 * returns List
 **/
exports.shortinfoGET = function (id, isDeleted) {
  return new Promise(function (resolve, reject) {
    var db = {};
    db['application/json'] = require('../db/employees');

    var employees = db['application/json'].filter((item) =>
      id.includes(item.id)
    );
    if (employees.length > 0) {
      resolve(employees);
    } else {
      resolve([]);
    }
  });
};

/**
 * получение информации о сотруднике
 *
 * name String ФИО сотрудника
 * returns List
 **/
exports.shortinfoNameGET = function (name) {
  return new Promise(function (resolve, reject) {
    var db = {};
    db['application/json'] = require('../db/employees');

    var employees = db['application/json'].filter((item) =>
      name.includes(item.name)
    );
    if (employees.length > 0) {
      resolve(employees);
    } else {
      resolve([]);
    }
  });
};
